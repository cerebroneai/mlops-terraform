variable "cloudwatch_name" {}
variable "sfn_name" {}

variable "role_step_arn" {}

variable "lambda_api_policy" {}

variable "lambda_api_role" {}

variable "lambda_filename" {}
variable "lambda_fn_name" {}

variable "api_gw_arname" {
    default = "arn:aws:execute-api:us-east-1:680763698946:aws_api_gateway_rest_api.api.id/*/aws_api_gateway_method.api-method.http_methodaws_api_gateway_resource.api-gw.path"
}

variable "dynamodb_name" {}
variable "billing_mode" {}
variable "region_name" {}