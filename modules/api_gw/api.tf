resource "aws_api_gateway_rest_api" "api" {
  name = "api"
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
resource "aws_api_gateway_resource" "api-gw" {
  parent_id   = aws_api_gateway_rest_api.api.root_resource_id
  path_part   = "path"
  rest_api_id = aws_api_gateway_rest_api.api.id
}
resource "aws_api_gateway_method" "api-method" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.api-gw.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
}
resource "aws_api_gateway_integration" "api-int" {
  http_method             = aws_api_gateway_method.api-method.http_method
  resource_id             = aws_api_gateway_resource.api-gw.id
  rest_api_id             = aws_api_gateway_rest_api.api.id
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = var.lambdafunction_arn #aws_lambda_function.Lambda_API.invoke_arn
}
resource "aws_api_gateway_deployment" "api-dep" {
  rest_api_id = aws_api_gateway_rest_api.api.id
  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.api-gw.id,
      aws_api_gateway_method.api-method.id,
      aws_api_gateway_integration.api-int.id,
    ]))
  }
  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_api_gateway_stage" "gw-stage" {
  deployment_id = aws_api_gateway_deployment.api-dep.id
  rest_api_id   = aws_api_gateway_rest_api.api.id
  stage_name    = "stage-api"
}