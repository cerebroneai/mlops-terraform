resource "aws_dynamodb_table" "No-SQL" {
  name             = var.dynamodb_name
  hash_key         = "TransactionID"
  billing_mode     = var.billing_mode
  stream_enabled   = true
  stream_view_type = "NEW_AND_OLD_IMAGES"
  attribute {
    name = "TransactionID"
    type = "S"
  }
  /*replica {
    region_name = var.region_name
  }*/
}