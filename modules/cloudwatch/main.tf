resource "aws_cloudwatch_log_group" "step" {
  name =  var.cloudwatch_name

  tags = {
    Environment = "testing"
  }
}