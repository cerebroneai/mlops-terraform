
resource "aws_sfn_state_machine" "sfn_terraform" {
  name     =  var.sfn_name    #"sfn-terraform"
  role_arn =  var.role_step_arn           #"arn:aws:iam::680763698946:role/service-role/StepFunctions-testECStask-role-161d2d5c"

  definition = <<EOF
{
        "Version": "1.0",
        "Comment": "Run ECS/Fargate tasks",
        "TimeoutSeconds": 120,
        "StartAt": "RunTask",
        "States": {
          "RunTask": {
            "Type": "Task",
            "Resource": "arn:aws:states:::ecs:runTask.sync",
            "Parameters": {
              "LaunchType": "EC2",
              "Cluster": "arn:aws:ecs:us-east-1:680763698946:cluster/testecs",
              "TaskDefinition": "arn:aws:ecs:us-east-1:680763698946:task-definition//ecs-task2:1"
             
            },
            "Retry": [
              {
                "ErrorEquals": [
                  "States.TaskFailed"
                ],
                "IntervalSeconds": 10,
                "MaxAttempts": 3,
                "BackoffRate": 2
              }
            ],
            "End": true
          }
        }
      }
EOF


logging_configuration {
    log_destination        = "${var.log_arn}:*"
    include_execution_data = true
    level                  = "ERROR"
  }
}
