resource "aws_iam_policy" "lambda-api-policy" {
  name        =  var.lambda_api_policy #"lambda-api-policy"
  path        = "/"
  description = "IAM Policy for Lambda"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [{
        "Action": [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
        ],
        "Resource": "arn:aws:logs:us-east-1:680763698946:*",
        "Effect": "Allow"
    }]
}
EOF
}
resource "aws_iam_role" "lambda-api" {
  name =  var.lambda_api_role #"lambda-api"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
resource "aws_lambda_function" "LambdaAPI" {
  filename      = "./modules/lambda_function/${var.lambda_filename}.zip"
  function_name =  var.lambda_fn_name #"LambdaAPI"
  role          = aws_iam_role.lambda-api.arn
  handler       = "lambda_handler"
  runtime       = "python3.8"
}
resource "aws_iam_role_policy_attachment" "lambda_policy" {
  role       = aws_iam_role.lambda-api.name
  policy_arn = aws_iam_policy.lambda-api-policy.arn
}
resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.LambdaAPI.function_name
  principal     = "apigateway.amazonaws.com"
  
  source_arn =  "arn:aws:execute-api:us-east-1:680763698946:aws_api_gateway_rest_api.api.id/*/aws_api_gateway_method.api-method.http_methodaws_api_gateway_resource.api-gw.path" #var.api_gw_arn 
}
