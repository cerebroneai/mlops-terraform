provider "aws" {
  region = "us-east-1"
}

module "api_gw" {
  source = "./modules/api_gw"
  lambdafunction_arn = module.lambda_function.LambdaAPi
}
module "cloudwatch" {
  source = "./modules/cloudwatch"
  cloudwatch_name = var.cloudwatch_name
}

module "step_function" {
  source = "./modules/step_function"
  sfn_name = var.sfn_name
  role_step_arn = var.role_step_arn
  log_arn =  module.cloudwatch.cloudwatch_arn
}

module "lambda_function" {
  source = "./modules/lambda_function"
  lambda_api_policy = var.lambda_api_policy
  lambda_api_role = var.lambda_api_role
  lambda_filename = var.lambda_filename
  lambda_fn_name = var.lambda_fn_name
  #api_gw_arn = var.api_gw_arn
}

module "dynamodb" {
  source = "./modules/dynamodb"
  dynamodb_name = var.dynamodb_name
  billing_mode = var.billing_mode
  region_name = var.region_name
}