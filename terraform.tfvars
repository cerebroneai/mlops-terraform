cloudwatch_name = "test1"
sfn_name = "test-step"
role_step_arn = "arn:aws:iam::680763698946:role/service-role/StepFunctions-testECStask-role-161d2d5c"

lambda_api_policy = "lambda-api-policy"

lambda_api_role = "lambda-api"
lambda_filename = "code"
lambda_fn_name = "LambdaAPI"
dynamodb_name = "Transaction"
billing_mode = "PAY_PER_REQUEST"
region_name = "us-east-1"